/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.common;

import id.co.jmto.nots.plugin.JMTONotsServicePlugin;
import id.co.jmto.nots.plugin.enums.DefaultValue;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import verso.config.Config;
import verso.log.Logger;

/**
 *
 * @author RCS
 */
public class General extends Thread{
    public static String dbName = "jmto";
    public static String cModuleNamespace = "jmto-notification.config.";
    public static String cMailModuleNamespace = "jmto-notification.mail.";
    public static Logger logger = Logger.forClass(JMTONotsServicePlugin.class);
    public static Config config;
    public static byte cEndMessageByte = -0x01;
    public static String cQueryServiceIpKey = "query-service-ip";
    public static String cQueryServicePortKey = "query-service-port";
    public static String cStresTestKey = "stress-test-simulation"; 
    public static String cSignatureDefaultKey = "signature-default";
    public static String cSubjectResendKey = "subject-resend";
    public static String cFromKey = "from";
    
    
    public String getNameHandler() { 
        return DefaultValue.HandlerName.getStringValue();
    }
    
    public static String getStackTraceMessage(Exception e){
        StackTraceElement[] tElm = e.getStackTrace();
        String tValeMessage = "\n [Exception: " + e.getMessage() + " ]";
        int index = 0;
        for (StackTraceElement stackTraceElement : tElm) {
            if (index < 20) {
                String tClass = stackTraceElement.getClassName();
                String tMethode = stackTraceElement.getMethodName();
                int line = stackTraceElement.getLineNumber();
                String tFullMessage = "\n  [Stack trace: (" + tClass + ":" + tMethode + ":" + line + ")]";
                tValeMessage = tValeMessage.concat(tFullMessage);
            }
            index++;
        }
        return tValeMessage;
    }
    
    public static String cleanTextContent(String pText) {
        String tAsciiString;
        try {
            tAsciiString = (new String(pText.getBytes("ASCII")));
            if(tAsciiString.contains("?")){
                tAsciiString = (new String(pText.getBytes("ASCII"))).replace("?", "");
                temporaryLog("clean-text","Clean Text [ "+ pText+" ] to [ "+tAsciiString+" ]");
            }
            return tAsciiString;
        } catch (UnsupportedEncodingException ex) {
            temporaryLog("error-clean-text","Failed Clean Text [ "+ pText+" ] ." + getStackTraceMessage(ex));
        }
        return pText;
    }
    
    public static String leftPadding(String pParam, int pLength, String pPad){
        String tValue = pParam;
        if(tValue.length() > pLength){
            tValue = tValue.substring(0, pLength);
        }
        
        while (tValue.length() < pLength) {            
            tValue = pPad+tValue;
        }
        
        return tValue;
    }
    
    public static String rightPadding(String pParam, int pLength, String pPad){
        String tValue = pParam;
        if(tValue.length() > pLength){
            tValue = tValue.substring(0, pLength);
        }
        
        while (tValue.length() < pLength) {            
            tValue = tValue+pPad;
        }
        
        return tValue;
    }
    
    public static String formatDateTime(String pDateTime, String pPatternInput, String pPatternExpected) {
        String formattedDate = "";

        try {
            SimpleDateFormat tDateYears = new SimpleDateFormat(pPatternInput);
            Date tDate = tDateYears.parse(pDateTime);
            tDateYears = new SimpleDateFormat(pPatternExpected);
            formattedDate = tDateYears.format(tDate);

        } catch (ParseException ex) {
        }

        return formattedDate;
    }

    public static String getStringConfig(String pKey){
        return config.getString(cModuleNamespace+pKey);
    }
    
    public static String getStringConfig(String configName, String pKey){
        return config.getString(configName+pKey);
    }
    
    public static int getIntConfig(String pKey){
        return config.getInt(cModuleNamespace+pKey);
    }
    
    public static int getIntConfig(String configName, String pKey){
        return config.getInt(configName+pKey);
    }
    
    public static boolean getBooleanConfig(String pKey){
        return config.getBoolean(cModuleNamespace+pKey);
    }
    
    public static boolean getBooleanConfig(String configName, String pKey){
        return config.getBoolean(configName+pKey);
    }
    
    public static void streamLog(String pMsg){
        logger.stream().log(pMsg);
    }
    
    public static void streamLog(String pMsg, Object... pObj){
        logger.stream().log(pMsg, pObj);
    }
    
    public static void traceLog(String pMsg){
        logger.trace().log(pMsg);
    }
    
    public static void traceLog(String pMsg, Object... pObject){
        logger.trace().log(pMsg, pObject);
    }
    
    public static void temporaryLog(String pName, String pMsg, Object... pObject){
        logger.temp(pName).log(pMsg, pObject);
    }
    
    public static void warningLog(String pMsg){
        logger.warning().log(pMsg);
    }
    
    public static void warningLog(String pMsg, Object... pObj){
        logger.warning().log(pMsg, pObj);
    }
    
    public static void warningLog(String pMsg, Exception e){
        logger.warning().log(pMsg, getStackTraceMessage(e));
    }
    
    public static void dbLog(String pMsg, Object... pObjec){
        logger.db().log(pMsg, pObjec);
    }
    
    public static void dbLog(String pMsg){
        logger.db().log(pMsg);
    }
    
    public static void dbErrorLog(String pMsg){
        logger.dbError().log(pMsg);
    }
    
    public static void dbErrorLog(String pMsg, Object... pObject){
        logger.dbError().log(pMsg, pObject);
    }
    
    public static void errorLog(String pMsg, Object... pObject){
        logger.error().log(pMsg, pObject);
    }
    
    public static void errorLog(String pMsg){
        logger.error().log(pMsg);
    }
    
    public static void errorLog(String pMsg, Exception e){
        logger.error().log(pMsg, getStackTraceMessage(e));
    }
}
