/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.mailsender;

import java.util.Arrays;

/**
 *
 * @author ajen
 */
public class MailRequest 
{
    private String[] mTo;
    private String mFrom = null;
    private String mSubject;
    private String mContentType;
    private String mContent;
    private String mSignature = "";
    private String mStringAtt = "";
    
    public static String cTypeStringAtt ="att/string";
    public static String cTypeTextHTML = "text/html"; 
    public static String cTypeTextPlain = "text/plain";
    
    public static String cFieldFC = "FC";
    public static String cFieldFN = "FN";
    
    public MailRequest(String[] pTo, String pFrom, String pSubject, String pContentType, String pContent, String pSign, String pStringAtt)
    {
        mTo = pTo;
        mSubject = pSubject;
        mContentType = pContentType==null?"text/plain":pContentType;
        mContent = pContent;
        mFrom = pFrom;
        mSignature = pSign;
        mStringAtt = pStringAtt;
    }
    
    public MailRequest(String[] pTo, String pFrom, String pSubject, String pContentType, String pContent, String pSign)
    {
        mTo = pTo;
        mSubject = pSubject;
        mContentType = pContentType==null?"text/plain":pContentType;
        mContent = pContent;
        mFrom = pFrom;
        mSignature = pSign;
    }
    
    public MailRequest(String[] pTo, String pFrom, String pSubject, String pContent, String pSign)
    {
        mTo = pTo;
        mSubject = pSubject;
        mContentType = "text/plain";
        mContent = pContent;
        mFrom = pFrom;
        mSignature = pSign;
    }
    
    public MailRequest(String[] pTo, String pSubject, String pContent)
    {
        mTo = pTo;
        mSubject = pSubject;
        mContentType = "text/plain";
        mContent = pContent;
        mSignature = "";
    }
    
    public MailRequest(String[] pTo, String pFrom, String pSubject, String pContent)
    {
        mTo = pTo;
        mSubject = pSubject;
        mContentType = "text/plain";
        mContent = pContent;
        mFrom = pFrom;
        mSignature = "";
    }

    /**
     * @return the mTo
     */
    public String[] getTo() {
        return mTo;
    }

    /**
     * @return the mSubject
     */
    public String getSubject() {
        return mSubject;
    }

    /**
     * @return the mContentType
     */
    public String getContentType() {
        return mContentType;
    }

    /**
     * @return the mContent
     */
    public String getContent() {
        return mContent;
    }

    /**
     * @return the mFrom
     */
    public String getFrom() {
        return mFrom;
    }
    
     /**
     * @return the mSignature
     */
    public String getSignature() {
        return mSignature;
    }
    
    @Override
    public String toString()
    {
        return "[From: "+mFrom+", To: "+Arrays.toString(mTo)+", Subject: "+mSubject+", Content Type: "+mContentType+", Content: "+mContent+" ]";
    }

    /**
     * @return the mStringAtt
     */
    public String getStringAtt() {
        return mStringAtt;
    }
}
