/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.enums.tables;

/**
 * @author  $Author:: ajen                                                      $: Author of last revision
 * @version $Revision:: 947                                                     $: Last revision number
 * @since   $LastChangedDate:: 2012-11-21 15:59:08 +0700 (Rabu, 21 Nop 2012)    $: Date of last revision
 */
public enum NotsLogs {

    TableName("APP_NOTIFICATION_LOG"),    
    ID("ID"),
    USER_ID("USER_ID"),
    MESSAGE_TYPE("MESSAGE_TYPE"),
    MSG_DEST("MSG_DEST"),
    CONTENT("CONTENT"),
    LOGGED("LOGGED"),
    WID("WID"),
    RC("RC"),
    ;
     

    private final Object value;
    private NotsLogs(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value.toString();
    }

    public String getFieldName() {
        return name();
    }
    
    public String getFieldName(boolean isLower) {
        if (isLower) {
            return name().toLowerCase();
        }
        return name();
    }
    
    public String getTableName() {
        return TableName.getValue();
    }
}
