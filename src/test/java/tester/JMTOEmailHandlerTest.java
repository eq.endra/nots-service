/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tester;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Base64;
import org.json.JSONObject;

/**
 *
 * @author AppaJrJr
 */
public class JMTOEmailHandlerTest
{
    private static BigInteger mSTANNumber = BigInteger.ZERO;    
    String mIP = "10.168.26.10";
    int mPort = 13120;
    String mFilePath = "struk-voucher.pdf";
   
    public static void main(String[] args) {
        new JMTOEmailHandlerTest().testEmailProtocol();
    }
    
    public void testEmailProtocol()
    {
        JSONObject tEmailRequest = new JSONObject();
        tEmailRequest.put("PAN", "JMNOTS");
        tEmailRequest.put("CONTENT_TYPE", "text/html");
        tEmailRequest.put("DT", "20150119143030");
        tEmailRequest.put("DESTNUM", "lutfi@vsi.co.id");
        tEmailRequest.put("CONTENT", "{\"TEMPLATE_KEY\":\"JMET001\", \"USER\":\"ASISSA\"}");
        tEmailRequest.put("MSGTYPE", "0");
        tEmailRequest.put("USERID", "viossbe");
        tEmailRequest.put("MC", "SEND");
        
        System.out.println("testing stream [" + tEmailRequest.toString() + "]");
        
        String tRawMessage = tEmailRequest.toString();
        System.out.println("request: " + tRawMessage);
        String tResponse = sendAndReceive(tRawMessage);

        JSONObject tResp = new JSONObject(tResponse);
        System.out.println("response: " + tResp);
        System.out.println("RC[" + (tResp.has("RC") ? tResp.getString("RC") : "9999" )+"]");
        
    }
    
    public void testAppsProtocol()
    {
        JSONObject tEmailRequest = new JSONObject();
        tEmailRequest.put("PAN", "JMNOTS");
        tEmailRequest.put("DT", "20150119143030");
        tEmailRequest.put("CONTENT", "{\"TEMPLATE_KEY\":\"JMET001\", \"USER\":\"ASISSA\"}");
        tEmailRequest.put("MSGTYPE", "1");
        tEmailRequest.put("USERID", "viossbe");
        tEmailRequest.put("MC", "NOTSAPPS");
        
        System.out.println("testing stream [" + tEmailRequest.toString() + "]");
        
        String tRawMessage = tEmailRequest.toString();
        System.out.println("request: " + tRawMessage);
        String tResponse = sendAndReceive(tRawMessage);

        JSONObject tResp = new JSONObject(tResponse);
        System.out.println("response: " + tResp);
        System.out.println("RC[" + (tResp.has("RC") ? tResp.getString("RC") : "9999" )+"]");
        
    }
    
    protected String getEncodedFile()
    {
        String encodedfile = null;
        File file = new File(mFilePath);
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = Base64.getEncoder().encodeToString(bytes);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return encodedfile;
    }
    
    protected String sendAndReceive(String pRequest) {        
        return sendAndReceive(mIP, mPort, pRequest);
    }
    
    protected static String sendAndReceive(String pIP, int pPort, String pRequest) {
        //setup connection
        Socket tSocket = new Socket();
        String tReturn = null;
        try {
            InetSocketAddress tTargetAddr = new InetSocketAddress(pIP, pPort);
            tSocket.connect(tTargetAddr, 10000);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            byte[] tData = new byte[pRequest.length() + 1];
            for (int i = 0; i < pRequest.length(); ++i) {
                tData[i] = (byte) pRequest.charAt(i);
            }
            tData[tData.length - 1] = -0x1;

            tSocket.getOutputStream().write(tData);

            StringBuilder tResponseStr = new StringBuilder();
            byte b;
            while ((b = (byte) tSocket.getInputStream().read()) != -1) {
                char c = (char) b;
                tResponseStr.append(c);
            }
            tSocket.close();
            tReturn = tResponseStr.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return tReturn;
    }
    
//    public static void main(String args[]){
//        String a="{\"PAN\":\"NOTS\",\"SIGN\":\"\",\"FLASH\":\"0\",\"REPORT\":\"0\",\"DT\":\"20161025194130\",\"PWD\":\"afafd8bb3e948965a9b2e0f9b4ca580b\",\"DESTNUM\":\"6282336466600\",\"CONTENT\":\"2016-10-25 19:41:29\\n0MSC2535166317D26E26F7DBB054BCC8\\nPENYAMBUNGAN BARU\\n5165012027390\\n20161023\\nGUDANG - H. DULHADI\\nRp.866.000,-\\nRp.2.500,-\\nSUKSES\",\"USE_TEMPLATE\":\"true\",\"SIGNATURE\":\"\",\"UID\":\"000000000654\",\"PROFID\":\"VIOSS00\",\"MSGTYPE\":\"0\",\"TEMPLATE_HANDLER\":\"\",\"MSGLEN\":\"1\",\"USERID\":\"viossbe\",\"BATCH\":\"1\"}";
//        sendAndReceive("10.25.50.20", 13016, a);
//    }
}