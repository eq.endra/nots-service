/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.enums.tables;

/**
 * @author  $Author:: ajen                                                      $: Author of last revision
 * @version $Revision:: 947                                                     $: Last revision number
 * @since   $LastChangedDate:: 2012-11-21 15:59:08 +0700 (Rabu, 21 Nop 2012)    $: Date of last revision
 */
public enum DBEmailSent {

    TableName("APP_EMAIL_SENT"),
    MAIL_TO("MAIL_TO"),
    CONTENT_HTML("CONTENT_HTML"),
    CONTENT_CSS("CONTENT_CSS"),
    MAIL_SUBJECT("MAIL_SUBJECT"),
    STATUS("STATUS"),
    CREATED_AT("CREATED_AT"),
    ;
     

    private final Object value;
    private DBEmailSent(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value.toString();
    }

    public String getFieldName() {
        return name();
    }
    
    public String getTableName() {
        return TableName.getValue();
    }
}
