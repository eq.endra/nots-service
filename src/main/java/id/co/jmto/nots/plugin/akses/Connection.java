/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.akses;

import id.co.jmto.nots.plugin.common.General;
import static id.co.jmto.nots.plugin.common.General.dbErrorLog;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testcontainers.shaded.okhttp3.MediaType;
import org.testcontainers.shaded.okhttp3.OkHttpClient;
import org.testcontainers.shaded.okhttp3.Request;
import org.testcontainers.shaded.okhttp3.RequestBody;
import org.testcontainers.shaded.okhttp3.Response;
import verso.loader.AppException;
import verso.message.ResponseCode;

/**
 *
 * @author RCS
 */
public class Connection extends General {
    
    public static void sendAsync(String pDBFunction, JSONObject pParam) {
        new Thread(() -> {
            JSONObject tResponse = postgreSQL(pDBFunction, pParam);
            if (!tResponse.getString("RC").equalsIgnoreCase(ResponseCode.SUCCESS.getResponseCodeString())) {
                dbErrorLog("Get Response not success for DB. Response: {}", tResponse.toString());
            }
        }).start();
    }
    
    public static void sendsync(String pDBFunction, JSONObject pParam) {
        JSONObject tResponse = postgreSQL(pDBFunction, pParam);
        if (!tResponse.getString("RC").equalsIgnoreCase(ResponseCode.SUCCESS.getResponseCodeString())) {
            dbErrorLog("Get Response not success for DB. Response: {}", tResponse.toString());
            throw new AppException(ResponseCode.ERROR_DATABASE, "Failed On insert notification");
        }
    }

    public static JSONObject getJSONObjectValue(String pDBFunction, JSONObject pParam) {
        JSONObject tResponse = postgreSQL(pDBFunction, pParam);
        if (!tResponse.getString("RC").equalsIgnoreCase(ResponseCode.SUCCESS.getResponseCodeString()) && !tResponse.getString("RC").equalsIgnoreCase(ResponseCode.ERROR_UNKNOWN_SUBSCRIBER.getResponseCodeString())) {
            throw new AppException(ResponseCode.ERROR_DATABASE, "Get Response not success for DB. Response: " + tResponse.toString());
        }

        if (tResponse.has("o")) {
            JSONArray tArrayOutput = new JSONArray(tResponse.get("o").toString());
            if (tArrayOutput.length() > 0) {
                JSONObject tOutput = new JSONObject(tArrayOutput.get(0).toString());
                return tOutput;
            }
        }

        return null;
    }

    public static JSONArray getJSONArrayValue(String pDBFunction, JSONObject pParam) {
        JSONObject tResponse = postgreSQL(pDBFunction, pParam);
        if (!tResponse.getString("RC").equalsIgnoreCase(ResponseCode.SUCCESS.getResponseCodeString()) && !tResponse.getString("RC").equalsIgnoreCase(ResponseCode.ERROR_UNKNOWN_SUBSCRIBER.getResponseCodeString())) {
            throw new AppException(ResponseCode.ERROR_DATABASE, "Get Response not success for DB. Response: " + tResponse.toString());
        }

        if (tResponse.has("o")) {
            JSONArray tOutput = new JSONArray(tResponse.get("o").toString());
            return tOutput;
        }
        
        return null;
    }

    public static JSONObject postgreSQL(String pDBFuncName, JSONObject pParam) {
        JSONObject tMessage = new JSONObject();
        tMessage.put("PAN", "11000");
        tMessage.put("f", pDBFuncName);
        tMessage.put("i", pParam);
        String tDBMessage = tMessage.toString();
        String tResponse;
        JSONObject tDBResponse;

        String tQSIP = getStringConfig(cQueryServiceIpKey);
        int tQSPort = getIntConfig(cQueryServicePortKey);

        InetSocketAddress mSocketAddress = new InetSocketAddress(tQSIP, tQSPort);

        tResponse = sendAndReceiveDB(mSocketAddress, tDBMessage);

        tDBResponse = new JSONObject(tResponse);

        return tDBResponse;
    }

    public static JSONObject sendMessage(InetSocketAddress pInetSocket, JSONObject pRequest) {
        try {
            JSONObject tResponse = sendAndReceiveJSONObject(pInetSocket, pRequest);
            return tResponse;
        } catch (ParseException ex) {
            throw new AppException(ResponseCode.ERROR_TIMEOUT, "Cannot Parese ISO8583 WIth Response");
        }
    }

    private static String sendAndReceiveDB(InetSocketAddress pInetSocket, String pRequest) {
        String tReturn = null;
        InetSocketAddress tTargetAddr = pInetSocket;
        try {
            dbLog("REQUEST to DB [" + tTargetAddr + "] : {}", pRequest);
            tReturn = sendMessageRaw(pRequest, tTargetAddr, 20000);
        } catch (IOException ex) {
            throw new AppException(ResponseCode.ERROR_DATABASE, "Exception in sending DB request: " + pRequest, ex);
        }

        dbLog("RESPONSE from DB [" + tTargetAddr + "] : " + tReturn);

        return tReturn;
    }

    private static JSONObject sendAndReceiveJSONObject(InetSocketAddress pInetSocket, JSONObject pRequest) throws ParseException {
        //setup connection
        String tReturn = null;
        InetSocketAddress tTargetAddr = pInetSocket;
        try {
            streamLog("GATEWAY REQUEST to " + tTargetAddr + " : {}", pRequest.toString());
            tReturn = sendMessageRaw(pRequest.toString(), tTargetAddr, 20000);
        } catch (IOException ex) {
            throw new AppException(ResponseCode.ERROR_DATABASE, "Exception in sending DB request: " + pRequest, ex);
        }

        JSONObject tResponse = new JSONObject(tReturn);
        streamLog("GATEWAY RESPONSE from " + tTargetAddr + " : {}", tResponse.toString());

        return tResponse;
    }

    private static String sendMessageRaw(String pMessageStream, InetSocketAddress pSocketAddress, int pTimeout) throws IOException {
        Socket tSocket = new Socket();
        tSocket.setSoTimeout(pTimeout);
        tSocket.connect(pSocketAddress, pTimeout);

        ByteArrayOutputStream tRequestByteStream = new ByteArrayOutputStream();
        tRequestByteStream.write(pMessageStream.getBytes());
        tRequestByteStream.write(cEndMessageByte);

        tSocket.getOutputStream().write(tRequestByteStream.toByteArray());

        String tResponseStream = "";
        byte tMessageByte;
        try {
            while ((tMessageByte = (byte) tSocket.getInputStream().read()) != cEndMessageByte) {
                tResponseStream += (char) tMessageByte;
            }
            if (tResponseStream.length() <= 0) {
                throw new AppException(ResponseCode.ERROR_TIMEOUT, "Response stream length <=0. read [" + tResponseStream + "] at incoming socket from " + pSocketAddress);
            }
        } catch (IOException ex) {
            try {
                tSocket.close();
                tSocket = null;
            } catch (IOException ex2) {
            }
            throw new AppException(ResponseCode.ERROR_TIMEOUT, "IOException on SendMessage() when reading response stream at incoming socket from " + pSocketAddress + ". Read message so far : [" + tResponseStream + "]", ex);
        }

        for (int i = 0; i < 10 && tSocket != null; i++) {
            try {
                tSocket.close();
                tSocket = null;
            } catch (IOException ex) {
                warningLog("Warning : Cannot close socket " + tSocket + " in attempt #" + i, ex);
            }
        }

        return tResponseStream;
    }

    public static String sendOKHttpPOSTRequest(String pParam, String pUrl) {
        OkHttpClient tCon;
        String tResponse = "";
        String tBaseUrl = pUrl;
        int timeout = getIntConfig("timeout");
        try {
            streamLog("GATEWAY REQUEST to " + tBaseUrl + ": " + pParam);
            tCon = new OkHttpClient().newBuilder().connectTimeout(timeout, TimeUnit.MILLISECONDS).build();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, pParam);
            Request request = new Request.Builder()
                    .url(tBaseUrl)
                    .method("POST", body)
                    .build();
            Response response = tCon.newCall(request).execute();
            tResponse = response.body().string();
            streamLog("GATEWAY RESPONSE from " + tBaseUrl + " [" + response.isSuccessful() + "]" + ": " + tResponse);
        } catch (IOException e) {
            errorLog("[ERROR] on Sending Request To Routing Service. {}", e);
            throw new AppException(ResponseCode.ERROR_TIMEOUT, "Time Out Gateway", e);
        }
        return tResponse;
    }

}
