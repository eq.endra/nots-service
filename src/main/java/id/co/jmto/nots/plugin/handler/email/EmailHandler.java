/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.handler.email;

import id.co.jmto.nots.plugin.common.General;
import id.co.jmto.nots.plugin.enums.NotificationMessage;
import id.co.jmto.nots.plugin.handler.JMTONotifcationHandler;
import id.co.jmto.nots.plugin.mailsender.MailRequest;
import id.co.jmto.nots.plugin.mailsender.MailSender;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import verso.loader.AppException;
import verso.message.ResponseCode;

/**
 *
 * @author RCS
 */
public class EmailHandler extends JMTONotifcationHandler {

    public EmailHandler(JSONObject pRequest) {
        super(pRequest);
    }

    @Override
    public JSONObject performHandler(JSONObject pRequestMessage) throws JSONException {
        String tContentkey = pRequestMessage.get(NotificationMessage.CONTENT_EMAIL.getFieldName()).toString();
        String tAttachment = null;
        String tSubject = "";

        List<JMTOEmailTemplater> tSubPluginLoaded = getTemplateLoaderList();

        JSONObject tContent = new JSONObject(tContentkey);
        
        if (tSubPluginLoaded != null) {
            boolean isLoaded = false;
            for (JMTOEmailTemplater tPlugin : tSubPluginLoaded) {
                if (tPlugin.canHandleRequest(tContent)) {
                    tContentkey = tPlugin.constructEmailNotificationContent(tContent);
                    tSubject = tPlugin.getTemplateKey().getSubject();
                    isLoaded = true;
                    break;
                }
            }
            
            if (!isLoaded) {
                throw new AppException(ResponseCode.ERROR_INVALID_KEY, "Cannot Find Template For Request: "+ tContent.getString("TEMPLATE_KEY"));
            }
        }else{
            JMTOEmailTemplater tPlugin = getTemplateWithKey(tContent.getString("TEMPLATE_KEY"));
            tContentkey = tPlugin.constructEmailNotificationContent(tContent);
            tSubject = tPlugin.getTemplateKey().getSubject();
        }
        
        if(tContent.has("SUBJECT")){
            tSubject = tContent.get("SUBJECT").toString();
        }

        General.traceLog("RequestMessage: " + pRequestMessage);
        General.traceLog("Content: " + tContentkey);
        String[] tArrContent = tContentkey.split("[|]");
        tContentkey = tArrContent[0];
        General.traceLog("Contentkey: " + tArrContent.length);
        if (tArrContent.length > 1) {
            tAttachment = tArrContent[1];
        }

        if (tArrContent.length > 2)
        {
            tSubject = tSubject.replace("#replacesubject#", tArrContent[2]);
            pRequestMessage.put(NotificationMessage.SUBJECT.getFieldName(), tSubject);
        }

        // construct mail request
        String[] tDest = tContent.getString(NotificationMessage.DESTNUM.getFieldName()).split("[;]");
        String tFrom = getStringConfig(cMailModuleNamespace, cFromKey);
        MailRequest tMail = new MailRequest(tDest,
                tFrom,
                tSubject,
                (pRequestMessage.has(NotificationMessage.CONTENT_TYPE.getFieldName()) ? pRequestMessage.getString(NotificationMessage.CONTENT_TYPE.getFieldName()) : MailRequest.cTypeTextHTML),
                tContentkey,
                pRequestMessage.has(NotificationMessage.SIGNATURE.getFieldName()) ? pRequestMessage.getString(NotificationMessage.SIGNATURE.getFieldName())
                : getStringConfig(cMailModuleNamespace, cSignatureDefaultKey),
                (tAttachment != null ? tAttachment : "")
        );

        traceLog("Mail Request: " + tMail.toString());
        MailSender.getInstance().sendEmail(pRequestMessage, tMail);

        JSONObject tRespMessage = pRequestMessage;

        tRespMessage.put(NotificationMessage.RC.getFieldName(), ResponseCode.SUCCESS.getResponseCodeString());
        tRespMessage.put("RCM", "SUKSES");

        return tRespMessage;
    }

}
