/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin;

import id.co.jmto.nots.plugin.akses.QueryService;
import id.co.jmto.nots.plugin.common.General;
import id.co.jmto.nots.plugin.common.TemplateField;
import id.co.jmto.nots.plugin.common.TemplateKey;
import id.co.jmto.nots.plugin.handler.JMTONotifcationHandler;
import id.co.jmto.nots.plugin.handler.email.JMTOEmailTemplater;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import org.json.JSONArray;
import org.json.JSONObject;
import verso.config.Config;
import verso.loader.service.Service;

/**
 *
 * @author RCS
 */
public class JMTOTemplateLoader implements Runnable, Service  {

    @Inject
    public JMTOTemplateLoader(Config config) {
        General.config = config;
    }
    
    @Override
    public void run() {
        JSONArray tTemplateDate = QueryService.selectTemplateHandler();
        List<JMTOEmailTemplater> tList = new ArrayList<>();
        for (int i = 0; i < tTemplateDate.length(); i++) {
            JSONObject tDataField = tTemplateDate.getJSONObject(i);
            String tCode = tDataField.getString("code");
            String tName = tDataField.getString("name");
            String tHtmlTemplate = tDataField.getString("content_html");
            String tCssTemplate = tDataField.getString("content_css");
            String tSubject = tDataField.getString("subject");
            String tAttachment = tDataField.getString("attachment");
            String tFieldData = tDataField.getString("syntax_fields");
            String[] tFieldList = tFieldData.split(";");
            
            LinkedList<TemplateField> tTemplateFiledLiist = new LinkedList<>();
            for (String fieldtemplate : tFieldList) {
                TemplateField field = new TemplateField(fieldtemplate);
                tTemplateFiledLiist.add(field);
            }
            tList.add(new JMTOEmailTemplater(new TemplateKey(tTemplateFiledLiist, tCode, tName, tSubject, tHtmlTemplate, tCssTemplate, tAttachment)));
        }
        
        General.traceLog("Init Notification Template Handler: "+ tList);
        JMTONotifcationHandler.setTemplateLoaderList(tList);
    }

    @Override
    public CompletableFuture<Void> start() {
        return CompletableFuture.runAsync(this);
    }

    @Override
    public CompletableFuture<Void> stop() {
        return null;
    }
    
    public static JMTOTemplateLoader initClass(){
        return new JMTOTemplateLoader(General.config);
    }
}
