/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.enums;

import java.util.EnumSet;

public enum NotificationMessage
{
    PAN,
    DT,
    RC,
    USERID,
    MSGTYPE,
    DESTNUM,
    LABEL,
    FROM,
    SUBJECT,
    PWD,
    CONTENT,
    CONTENT_TYPE,
    CONTENT_ATTACHMENT,
    FC,
    PROFID,
    SIGNATURE,
    MC,
    TEMPLATE_KEY,
    GROUP_NOTIF_ID,
    CONTENT_EMAIL,
    CONTENT_WEB_APP,
    CONTENT_SMS,
    ;
   
    public static EnumSet<NotificationMessage> getBasicRequestKey() {
        return EnumSet.of(PAN,
                          DT,
                          MSGTYPE
                         );
    }

    public static EnumSet<NotificationMessage> getEmailKey() {
        EnumSet<NotificationMessage> tKeys = getBasicRequestKey();
        tKeys.addAll(EnumSet.of(
                                CONTENT_EMAIL
                     ));
        return tKeys;
    }
    
    public static EnumSet<NotificationMessage> getEmailContentKey() {
        return EnumSet.of(TEMPLATE_KEY, DESTNUM);
    }
    
    public static EnumSet<NotificationMessage> getWebAppNotsKey() {
        EnumSet<NotificationMessage> tKeys = getBasicRequestKey();
        tKeys.addAll(EnumSet.of(
                                CONTENT_WEB_APP
                     ));
        return tKeys;
    }
    
    public static EnumSet<NotificationMessage> getSMSNotsKey() {
        EnumSet<NotificationMessage> tKeys = getBasicRequestKey();
        tKeys.addAll(EnumSet.of(
                                CONTENT_SMS
                     ));
        return tKeys;
    }
    
    public static EnumSet<NotificationMessage> getWebContentKey() {
        return EnumSet.of(TEMPLATE_KEY, GROUP_NOTIF_ID);
    }
    
    public static EnumSet<NotificationMessage> getSMSContentKey() {
        return EnumSet.of(TEMPLATE_KEY, DESTNUM);
    }
    
    public static EnumSet<NotificationMessage> getEmailWebAppNotsKey() {
        EnumSet<NotificationMessage> tKeys = getBasicRequestKey();
        tKeys.addAll(EnumSet.of(                            
                                CONTENT_WEB_APP,
                                CONTENT_EMAIL
                     ));
        return tKeys;
    }

    public String getFieldName() {
        return name();
    }

}
