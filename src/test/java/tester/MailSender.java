/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tester;

import id.co.jmto.nots.plugin.mailsender.*;
import id.co.jmto.nots.plugin.akses.QueryService;
import id.co.jmto.nots.plugin.common.General;
import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import verso.loader.AppException;
import verso.message.ResponseCode;

public class MailSender {

    int mHostPort;
    String mHostName;
    private String mUserName;
    private String mPassword;

    public MailSender() {
        try {
            mHostName = "mail.jmto.co.id";
            mHostPort = 110;
            mUserName = "supportpg@jmto.co.id";
            mPassword = "supportpg!@#";
            System.out.println("init mail sender " + this);
        } catch (Exception e) {
            throw new AppException(ResponseCode.ERROR_TIMEOUT, "Failed Init Mail Sender", e);
        }
    }

    public MailSender(String pHostName, int pPort) {
        mHostName = pHostName;
        mHostPort = pPort;
    }

    public void sendEmail(JSONObject pRequestMessage, MailRequest pMail) {
        try {
            Properties props = new Properties();

            props.put("mail.smtp.host", mHostName);
            props.put("mail.smtp.port", mHostPort);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "false");

            Session tMailSession = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(mUserName, mPassword);
                }
            });
            MimeMessage tMessage = new MimeMessage(tMailSession);
            tMessage.setSubject(pMail.getSubject());
            String tContent = pMail.getContent();
            tMessage.setFrom(new InternetAddress(pMail.getFrom()));
            for (String to : pMail.getTo()) {
                tMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            }

            if (pMail.getContentType().equalsIgnoreCase(MailRequest.cTypeTextHTML)) {
                tContent = tContent + "<br />" + pMail.getSignature();
                tMessage.setContent(tContent, pMail.getContentType());
            } else if (pMail.getContentType().equalsIgnoreCase(MailRequest.cTypeTextPlain)) {
                tContent = tContent + "\n\n" + pMail.getSignature();
                tMessage.setContent(tContent, pMail.getContentType());
            } else if (pMail.getContentType().equalsIgnoreCase(MailRequest.cTypeStringAtt)) {
                MimeBodyPart tMessageBodyPart = new MimeBodyPart();
                String tContentBody = tContent + "\n\n" + pMail.getSignature();
                tMessageBodyPart.setContent(tContentBody, MailRequest.cTypeTextHTML);
                Multipart multipart = new MimeMultipart("mixed");
                multipart.addBodyPart(tMessageBodyPart);

                JSONArray tFileArr = new JSONArray(pMail.getStringAtt());
                for (int i = 0; i < tFileArr.length(); i++) {
                    MimeBodyPart tPdf = new MimeBodyPart();
                    String tFileContent = tFileArr.getJSONObject(i).getString(MailRequest.cFieldFC);
                    DataSource tAttachment = new ByteArrayDataSource(Base64.getDecoder().decode(tFileContent), "application/pdf");
                    tPdf.setDataHandler(new DataHandler(tAttachment));
                    tPdf.setFileName(tFileArr.getJSONObject(i).getString(MailRequest.cFieldFN));
                    multipart.addBodyPart(tPdf);
                }
                tMessage.setContent(multipart);
            }

            Transport.send(tMessage);
            System.out.println("Finish send email [" + mHostName + ":" + mHostPort + "] to: " + Arrays.toString(pMail.getTo()));
        } catch (MessagingException | JSONException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Hostname: " + mHostName + ", port: " + mHostPort;
    }

    private static class SingletonHolder {

        private static final MailSender INSTANCE = new MailSender();
    }

    public static MailSender getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public static void main(String[] args) {
        MailRequest request = new MailRequest(new String[]{"lutfiriyanto77@gmail.com","lutfi@recis.co.id"}, "PG Support <supportpg@jmto.co.id>", "Testing", "Halo , Ini Email test yahhh");
        new MailSender().sendEmail(null, request);
    }
}
