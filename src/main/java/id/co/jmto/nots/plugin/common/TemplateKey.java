/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.common;

import java.util.LinkedList;

/**
 *
 * @author RCS
 */
public class TemplateKey {
    private final LinkedList<TemplateField> mFieldList;
    private final String mKey, mName, mSubject, mHTMLTemplate, mCSSTemplate, mAttachment;

    public TemplateKey(LinkedList<TemplateField> mFieldList, String mKey, String mName, String mSubject, String mHTMLTemplate, String mCSSTemplate, String mAttachment) {
        this.mFieldList = mFieldList;
        this.mKey = mKey;
        this.mName = mName;
        this.mSubject = mSubject;
        this.mHTMLTemplate = mHTMLTemplate;
        this.mCSSTemplate = mCSSTemplate;
        this.mAttachment = mAttachment;
    }

    public String getKey() {
        return mKey;
    }

    public LinkedList<TemplateField> getFieldList() {
        return mFieldList;
    }
    
    private String getHTMLTemplate(){
        return mHTMLTemplate;
    }

    public String getName() {
        return mName;
    }

    public String getSubject() {
        return mSubject;
    }

    private String getCSSTemplate() {
        return mCSSTemplate;
    }

    public String getAttachment() {
        return mAttachment;
    }

    public String getTemplate(){
        String ttemp = "<style type='text/css'>"+ getCSSTemplate()+"</style>";
        ttemp = getHTMLTemplate().replace("#style#", ttemp);
        return ttemp;
    }
    
    public String getTemplate(boolean isHtml){
        if (isHtml) {
            return getTemplate();
        }else{
            return getHTMLTemplate();
        }
    }
    
    @Override
    public String toString() {
        return "TemplateKey{" + "FieldList=" + mFieldList + ", Key=" + mKey + ", Name=" + mName + ", Subject=" + mSubject + ", HTMLTemplate=" + mHTMLTemplate + ", CSSTemplate=" + mCSSTemplate + ", Attachment=" + mAttachment + '}';
    }
}
