/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin;

import id.co.jmto.nots.plugin.akses.QueryService;
import id.co.jmto.nots.plugin.common.General;
import static id.co.jmto.nots.plugin.common.General.errorLog;
import id.co.jmto.nots.plugin.enums.DefaultValue;
import id.co.jmto.nots.plugin.enums.NotificationMessage;
import id.co.jmto.nots.plugin.handler.app.NotifAppslHandler;
import id.co.jmto.nots.plugin.handler.email.EmailHandler;
import id.co.jmto.nots.plugin.handler.sms.SMSHandler;
import java.util.EnumSet;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import verso.config.Config;
import verso.json.JsonPlugin;
import verso.loader.AppException;
import verso.message.ResponseCode;

/**
 *
 * @author RCS
 */
public class JMTONotsServicePlugin implements JsonPlugin {

    @Inject
    public JMTONotsServicePlugin(Config config) {
        General.config = config;
    }

    @Override
    public boolean canHandleRequest(JSONObject pRequest) {
        if (pRequest.get(NotificationMessage.PAN.getFieldName()).toString() != null && pRequest.getString(NotificationMessage.PAN.getFieldName()).equals(DefaultValue.PAN.getStringValue())) {
            return true;
        } else {
            General.errorLog("wrong PAN value got: {}", pRequest.getString(NotificationMessage.PAN.getFieldName()), "{} should be {}", DefaultValue.PAN.getStringValue());
        }
        return false;
    }
    
    public int validateMessage(JSONObject pRequestMessage) {
        EnumSet<NotificationMessage> tBasicRequet = NotificationMessage.getBasicRequestKey();
        
        validateMandatoryField(pRequestMessage, tBasicRequet);
        
        if (pRequestMessage.has(NotificationMessage.RC.getFieldName())) {
            throw new AppException(ResponseCode.ERROR_INVALID_MESSAGE, "message contain RC element");
        }

        int tMsgType = Integer.parseInt(pRequestMessage.get(NotificationMessage.MSGTYPE.getFieldName()).toString());
        if (tMsgType == 0 || tMsgType == 2 || tMsgType == 4) {
            JSONObject tEmailContent = pRequestMessage.getJSONObject(NotificationMessage.CONTENT_EMAIL.getFieldName());
            EnumSet<NotificationMessage> tContenEmail = NotificationMessage.getEmailContentKey();
            validateMandatoryField(tEmailContent, tContenEmail);
        }

        if (tMsgType == 1 || tMsgType == 2) {
            JSONObject tWebContent = pRequestMessage.getJSONObject(NotificationMessage.CONTENT_WEB_APP.getFieldName());
            EnumSet<NotificationMessage> tContenWeb = NotificationMessage.getEmailContentKey();
            validateMandatoryField(tWebContent, tContenWeb);
        }

        if (tMsgType == 3 || tMsgType == 4) {
            JSONObject tWebContent = pRequestMessage.getJSONObject(NotificationMessage.CONTENT_SMS.getFieldName());
            EnumSet<NotificationMessage> tConten = NotificationMessage.getSMSContentKey();
            validateMandatoryField(tWebContent, tConten);
        }
        
        return tMsgType;
    }
    
    private void validateMandatoryField(JSONObject pContent, EnumSet<NotificationMessage> pNotsMessage){
        pNotsMessage.forEach(key -> {
            if (!pContent.toMap().containsKey(key.name())) {
                throw new AppException(ResponseCode.ERROR_INVALID_MESSAGE, key + " is required");
            } else if (pContent.get(key.getFieldName()).toString().length() == 0) {
                throw new AppException(ResponseCode.ERROR_INVALID_MESSAGE, key.name() + " has empty value");
            }
        });
    }

    @Override
    public JSONObject handleRequest(JSONObject request) {
        try {
            JSONObject tNotsReq = request;
            int tMsgtype = validateMessage(tNotsReq);

            General.streamLog("DOWNLINE REQUEST : {}", tNotsReq);
            QueryService.logJSONMessage(tNotsReq);
            

            if (tMsgtype == 0 || tMsgtype == 2) {
                new EmailHandler(tNotsReq).handleMessage();
            }

            if (tMsgtype == 1 || tMsgtype == 2) {
                new NotifAppslHandler(tNotsReq).handleMessage();
            }
            
            if(tMsgtype == 3 || tMsgtype == 4){
                new SMSHandler(tNotsReq).handleMessage();
            }

            JSONObject tResponseMessage = tNotsReq;
            tResponseMessage.put(NotificationMessage.RC.getFieldName(), ResponseCode.SUCCESS.getResponseCodeString());
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseMessage);

            QueryService.logJSONMessage(tResponseMessage);
            return tResponseMessage;
        }  catch (AppException e) {
            errorLog("[AppException] Error on Handle Request: {}", e);
            ResponseCode tRC = e.getResponseCode();
            String tRCM = e.getMessage();
            
            if (tRC == ResponseCode.ERROR_DATABASE) {
                tRCM = "Internal Error";
            }
            JSONObject tResponseErrorMessage = request;
            tResponseErrorMessage.put(NotificationMessage.RC.getFieldName(), tRC.getResponseCodeString());
            tResponseErrorMessage.put("RCM", tRCM);

            // log message
            QueryService.logJSONMessage(tResponseErrorMessage);
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseErrorMessage);
            return tResponseErrorMessage;
        } catch (JSONException e) {
            errorLog("[JSONException] Error on Handle Request: {}", e);
            ResponseCode tRC = ResponseCode.ERROR_OTHER;
            String tRCM = "Error Lainya";
            JSONObject tResponseErrorMessage = request;
            tResponseErrorMessage.put(NotificationMessage.RC.getFieldName(), tRC.getResponseCodeString());
            tResponseErrorMessage.put("RCM", tRCM);

            // log message
            QueryService.logJSONMessage(tResponseErrorMessage);
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseErrorMessage);
            return tResponseErrorMessage;
        } catch (Exception e) {
            errorLog("[Exception] Error on Handle Request: {}", e);
            ResponseCode tRC = ResponseCode.ERROR_TIMEOUT;
            String tRCM = "Time Out";
            JSONObject tResponseErrorMessage = request;
            tResponseErrorMessage.put(NotificationMessage.RC.getFieldName(), tRC.getResponseCodeString());
            tResponseErrorMessage.put("RCM", tRCM);

            // log message
            QueryService.logJSONMessage(tResponseErrorMessage);
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseErrorMessage);
            return tResponseErrorMessage;
        }
    }

}
