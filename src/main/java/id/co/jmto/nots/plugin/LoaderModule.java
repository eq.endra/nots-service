package id.co.jmto.nots.plugin;

import verso.loader.module.AbstractPluginLoaderModule;

public class LoaderModule extends AbstractPluginLoaderModule<JMTONotsServiceHTTPPlugin> {
    
    @Override
    protected Class<JMTONotsServiceHTTPPlugin> getPluginClass() {
        return JMTONotsServiceHTTPPlugin.class;
    }

    @Override
    protected void initialize() {
//        bindService(JMTOTemplateLoader.class);
    }
}
