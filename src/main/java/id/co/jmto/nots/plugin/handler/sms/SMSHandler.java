/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.handler.sms;

import id.co.jmto.nots.plugin.handler.email.*;
import id.co.jmto.nots.plugin.enums.NotificationMessage;
import id.co.jmto.nots.plugin.handler.JMTONotifcationHandler;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import verso.message.ResponseCode;

/**
 *
 * @author RCS
 */
public class SMSHandler extends JMTONotifcationHandler {

    public SMSHandler(JSONObject pRequest) {
        super(pRequest);
    }

    @Override
    public JSONObject performHandler(JSONObject pRequestMessage) throws JSONException {
        FileWriter file;
        try {
            String tContentkey = pRequestMessage.get(NotificationMessage.CONTENT_SMS.getFieldName()).toString();
            JSONObject tContent = new JSONObject(tContentkey);
            JMTOEmailTemplater tPlugin = getTemplateWithKey(tContent.getString("TEMPLATE_KEY"));
            tContentkey = tPlugin.constructSmsNotificationContent(tContent);
            traceLog("RequestMessage: " + pRequestMessage);
            traceLog("Content: " + tContentkey);
            String tDestnum = tContent.get(NotificationMessage.DESTNUM.getFieldName()).toString();
            tDestnum = normalizeNumber(tDestnum);
            String tPoolPath = getStringConfig("jmto-notification.sms.", "spool-path");
            String tFlas = getStringConfig("jmto-notification.sms.", "flash");
            String tAlph = getStringConfig("jmto-notification.sms.", "alphabet");
            File tFileFolderOutgoing = new File(tPoolPath);
            if (!tFileFolderOutgoing.exists()) {
                tFileFolderOutgoing.mkdir();
            }   
            
            String tdate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            String tFileName = tDestnum+"-"+tdate;
            
            file = new FileWriter(new File(tPoolPath+"/"+tFileName));
            
            try (BufferedWriter writer = new BufferedWriter(file)) {
                writer.write("To: "+tDestnum+"\n");
                writer.write("Flash: "+tFlas+"\n");
                writer.write("Alphabet: "+tAlph+"\n\n");
                writer.write(tContentkey);
                writer.flush();
            }
            
            JSONObject tRespMessage = pRequestMessage;
            tRespMessage.put(NotificationMessage.RC.getFieldName(), ResponseCode.SUCCESS.getResponseCodeString());
            tRespMessage.put("RCM", "SUKSES");
            return tRespMessage;
        } catch (IOException ex) {
            errorLog("Error Handle SMS Handler to Create file {}", ex);
        }
        
        return pRequestMessage;
    }
    
    private String normalizeNumber(String pDestnum){
        pDestnum = pDestnum.replace("+", "").replace("-", "");
        if (pDestnum.startsWith("08")) {
            pDestnum = "62"+ pDestnum.substring(1, pDestnum.length());
        }
        
        return pDestnum;
    }

}
