/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */
package id.co.jmto.nots.plugin.handler.email;

import id.co.jmto.nots.plugin.common.TemplateField;
import id.co.jmto.nots.plugin.common.TemplateKey;
import java.math.BigDecimal;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author shiro
 */
public class JMTOEmailTemplater {
    
    public static String TEMPLATE_KEY = "TEMPLATE_KEY";

    private final TemplateKey mTemplateKey;

    public JMTOEmailTemplater(TemplateKey pTemplateKey) {
        this.mTemplateKey = pTemplateKey;
    }
    
    public String getTemplate(boolean isHtml) {
        return mTemplateKey.getTemplate(isHtml);
    }

    public String removePaddingNumber(String pStringAmount) {
        BigDecimal tAmount = new BigDecimal(pStringAmount);
        return tAmount.toPlainString();
    }

    public String constructEmailNotificationContent(JSONObject pRequestMessage) {
        String tEmailTemplate = getTemplate(true);
        String tEmail = constructContent(tEmailTemplate, pRequestMessage);
        return tEmail;
    }
    
    public String constructAppNotificationContent(JSONObject pRequestMessage) {
        String tEmailTemplate = getTemplate(false);
        String tEmail = constructContent(tEmailTemplate, pRequestMessage);
        return tEmail;
    }
    
    public String constructSmsNotificationContent(JSONObject pRequestMessage) {
        String tEmailTemplate = getTemplate(false);
        String tEmail = constructContent(tEmailTemplate, pRequestMessage);
        return tEmail;
    }

    public String constructContent(final String pTemplate, final JSONObject pRequest){
        String tContentValue = pTemplate;
        
        for (TemplateField key : mTemplateKey.getFieldList()) {
            tContentValue = tContentValue.replace(key.getReplacedField(), pRequest.has(key.getField()) ? pRequest.getString(key.getField()) : "");
        }
        
        boolean isListData = pRequest.has("LISTDATA") ? pRequest.getBoolean("LISTDATA") : false;
        
        String tValueData = "";
        if(isListData){
            if(pRequest.has("DATA")){
                if(pRequest.get("DATA") instanceof JSONArray){
                    JSONArray tListData = pRequest.getJSONArray("DATA");
                    for (int i=0; i< tListData.length(); i++) {
                        JSONObject tData = tListData.getJSONObject(i);
                        String tLabel = tData.get("LABEL").toString();
                        String tValue = tData.get("VALUE").toString();
                        
                        if (tLabel.equalsIgnoreCase("separator")) {
                            tValueData = tValueData.concat(tValue);
                        }else{
                            tValueData = tValueData.concat(tLabel).concat(" : ").concat(tValue).concat("<br>");
                        }
                    }
                }
            }
        }
        
        tContentValue = tContentValue.replace("#LISTOFINFO#", tValueData);
        return tContentValue;
    }

    protected static String generateVarLengthString(int pLength, char pChar) {
        StringBuilder tRes = new StringBuilder();
        for (int i = 0; i < pLength; i++) {
            tRes.append(pChar);
        }
        return tRes.toString();
    }

    protected static String leftPad(String pSource, int pLength, char pChar) {
        return generateVarLengthString(pLength - pSource.length(), pChar) + pSource;
    }

    protected static String rightPad(String pSource, int pLength, char pChar) {
        return pSource + generateVarLengthString(pLength - pSource.length(), pChar);
    }

    public boolean canHandleRequest(JSONObject pRequest){
        if(pRequest.has(TEMPLATE_KEY)){
            String key = pRequest.getString(TEMPLATE_KEY);
            return key.equals(mTemplateKey.getKey());
        }
        
        return false;
    }

    public TemplateKey getTemplateKey() {
        return mTemplateKey;
    }
    
}
