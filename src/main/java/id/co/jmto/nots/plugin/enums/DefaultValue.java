/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.enums;

public enum DefaultValue {
    MessageTypeEmail (0),
    MessageTypeAppNots (1),
    PAN ("JMNOTS"),
    SEND("SEND"),
    RESEND("RESEND"),
    NOTSAPPS("NOTSAPPS"),
    TIMEOUT (3000),
    StressTest ("false"),
    LoadedKey ("mysqlnots"),
    RouteOnlyYes ("y"),
    HandlerName ("NotificationHandler"),        
    StatusUserNonActive ("0"),        
    StatusUserActive ("1"),        
    StatusUserBlocked ("2"),            
    ;

    private final Object value;
    private DefaultValue(String value) {
        this.value = value;
    }

    private DefaultValue(int value) {
        this.value = value;
    }

    public String getStringValue() {
        return value.toString();
    }

    public int getIntegerValue() {
        return Integer.parseInt(value.toString());
    }
    
}
