/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin;

import com.google.common.net.HttpHeaders;
import id.co.jmto.nots.plugin.akses.QueryService;
import id.co.jmto.nots.plugin.common.General;
import static id.co.jmto.nots.plugin.common.General.errorLog;
import id.co.jmto.nots.plugin.enums.DefaultValue;
import id.co.jmto.nots.plugin.enums.NotificationMessage;
import id.co.jmto.nots.plugin.handler.app.NotifAppslHandler;
import id.co.jmto.nots.plugin.handler.email.EmailHandler;
import id.co.jmto.nots.plugin.handler.sms.SMSHandler;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.EnumSet;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.json.JSONException;
import org.json.JSONObject;
import verso.config.Config;
import verso.http.ClientMessageLoggerFilter;
import verso.http.HttpPlugin;
import verso.loader.AppException;
import verso.message.ResponseCode;

/**
 *
 * @author RCS
 */
@Path("")
public class JMTONotsServiceHTTPPlugin extends General implements HttpPlugin {

    private final Client client;

    @Inject
    public JMTONotsServiceHTTPPlugin(Config config) {
        General.config = config;
        client = ClientBuilder.newBuilder().register(JacksonFeature.class).register(new ClientMessageLoggerFilter(logger)).build();
    }

    @Produces(MediaType.APPLICATION_JSON)
    @POST
    @Path("/notification")
    public String sendNotification(String pBodyMessage, @Context HttpHeaders pHeaders) {
        return handleMessage(pBodyMessage);
    }
    
    private boolean validatePAN(JSONObject pRequest){
        if (pRequest.get(NotificationMessage.PAN.getFieldName()).toString() != null && pRequest.getString(NotificationMessage.PAN.getFieldName()).equals(DefaultValue.PAN.getStringValue())) {
            return true;
        } else {
            General.errorLog("wrong PAN value got: {}", pRequest.getString(NotificationMessage.PAN.getFieldName()), "{} should be {}", DefaultValue.PAN.getStringValue());
        }
        return false;
    }

    private String handleMessage(String request) {
        String tRequest = null;
        try {
            tRequest = URLDecoder.decode(request, StandardCharsets.UTF_8.toString());
            JSONObject tNotsReq = new JSONObject(tRequest);
            int tMsgtype = validateMessage(tNotsReq);
            
            General.streamLog("DOWNLINE REQUEST : {}", tNotsReq);
            if (!validatePAN(tNotsReq)) {
                return request;
            }
            
            QueryService.logJSONMessage(tNotsReq);

            if (tMsgtype == 0 || tMsgtype == 2 || tMsgtype == 4) {
                new EmailHandler(tNotsReq).handleMessage();
            }

            if (tMsgtype == 1 || tMsgtype == 2) {
                new NotifAppslHandler(tNotsReq).handleMessage();
            }

            if(tMsgtype == 3 || tMsgtype == 4){
                new SMSHandler(tNotsReq).handleMessage();
            }
            
            JSONObject tResponseMessage = tNotsReq;
            tResponseMessage.put(NotificationMessage.RC.getFieldName(), ResponseCode.SUCCESS.getResponseCodeString());
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseMessage);

            QueryService.logJSONMessage(tResponseMessage);
            return tResponseMessage.toString();
        } catch (AppException ex) {
            errorLog("[AppException] Error on Handle Request: {}", ex);
            ResponseCode tRC = ex.getResponseCode();
            String tRCM = ex.getMessage();
            
            if (tRC == ResponseCode.ERROR_DATABASE) {
                tRCM = "Internal Error";
            }
            
            JSONObject tResponseErrorMessage = new JSONObject(tRequest);
            tResponseErrorMessage.put(NotificationMessage.RC.getFieldName(), tRC.getResponseCodeString());
            tResponseErrorMessage.put("RCM", tRCM);

            // log message
            QueryService.logJSONMessage(tResponseErrorMessage);
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseErrorMessage);            
            return tResponseErrorMessage.toString();
        } catch (UnsupportedEncodingException | JSONException e) {
            errorLog("[Execption] Error on Handle Request: {}", e);
            ResponseCode tRC = ResponseCode.ERROR_OTHER;
            String tRCM = "Error Lainya";
            JSONObject tResponseErrorMessage = new JSONObject(tRequest);
            tResponseErrorMessage.put(NotificationMessage.RC.getFieldName(), tRC.getResponseCodeString());
            tResponseErrorMessage.put("RCM", tRCM);

            // log message
            QueryService.logJSONMessage(tResponseErrorMessage);
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseErrorMessage);            
            return tResponseErrorMessage.toString();
        } catch (Exception e) {
            errorLog("[Execption] Error on Handle Request: {}", e);
            ResponseCode tRC = ResponseCode.ERROR_TIMEOUT;
            String tRCM = "Time Out";
            JSONObject tResponseErrorMessage = new JSONObject(tRequest);
            tResponseErrorMessage.put(NotificationMessage.RC.getFieldName(), tRC.getResponseCodeString());
            tResponseErrorMessage.put("RCM", tRCM);

            // log message            
            QueryService.logJSONMessage(tResponseErrorMessage);
            General.streamLog("DOWNLINE RESPONSE : {}", tResponseErrorMessage);
            return tResponseErrorMessage.toString();
        }

    }

    public int validateMessage(JSONObject pRequestMessage) {
        EnumSet<NotificationMessage> tBasicRequet = NotificationMessage.getBasicRequestKey();
        validateMandatoryField(pRequestMessage, tBasicRequet);
        if (pRequestMessage.has(NotificationMessage.RC.getFieldName())) {
            throw new AppException(ResponseCode.ERROR_INVALID_MESSAGE, "message contain RC element");
        }

        int tMsgType = Integer.parseInt(pRequestMessage.get(NotificationMessage.MSGTYPE.getFieldName()).toString());
        if (tMsgType == 0 || tMsgType == 2 || tMsgType == 4) {
            JSONObject tEmailContent = pRequestMessage.getJSONObject(NotificationMessage.CONTENT_EMAIL.getFieldName());
            EnumSet<NotificationMessage> tContenEmail = NotificationMessage.getEmailContentKey();
            validateMandatoryField(tEmailContent, tContenEmail);
        }

        if (tMsgType == 1 || tMsgType == 2) {
            JSONObject tWebContent = pRequestMessage.getJSONObject(NotificationMessage.CONTENT_WEB_APP.getFieldName());
            EnumSet<NotificationMessage> tContenWeb = NotificationMessage.getWebContentKey();
            validateMandatoryField(tWebContent, tContenWeb);
        }
        
        if (tMsgType == 3 || tMsgType == 4) {
            JSONObject tWebContent = pRequestMessage.getJSONObject(NotificationMessage.CONTENT_SMS.getFieldName());
            EnumSet<NotificationMessage> tConten = NotificationMessage.getSMSContentKey();
            validateMandatoryField(tWebContent, tConten);
        }

        return tMsgType;
    }
 
    private void validateMandatoryField(JSONObject pContent, EnumSet<NotificationMessage> pNotsMessage){
        pNotsMessage.forEach(key -> {
            if (!pContent.toMap().containsKey(key.name())) {
                throw new AppException(ResponseCode.ERROR_INVALID_MESSAGE, key + " is required");
            } else if (pContent.get(key.getFieldName()).toString().length() == 0) {
                throw new AppException(ResponseCode.ERROR_INVALID_MESSAGE, key.name() + " has empty value");
            }
        });
    }
}
