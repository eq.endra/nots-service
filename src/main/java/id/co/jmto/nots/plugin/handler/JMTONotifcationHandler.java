/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.handler;

import id.co.jmto.nots.plugin.akses.QueryService;
import id.co.jmto.nots.plugin.common.General;
import id.co.jmto.nots.plugin.common.TemplateField;
import id.co.jmto.nots.plugin.common.TemplateKey;
import id.co.jmto.nots.plugin.handler.email.JMTOEmailTemplater;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author RCS
 */
public abstract class JMTONotifcationHandler extends General {

    public static String cFDBSelectProfile = "nots.selprofile";
    public static String cFDBSelectUser = "nots.q";
    public static String cFDBInsertLog = "nots.inlog";
    public static String cFDBSelectTemplate = "nots.seltemplate";
    public static String cFDBSelectTemplateByKey = "nots.seltemplatebykey";
    public static String cFDBInsertNotsWebApp = "nots.insnotwebapp";
    public static String cFDBSelectRoleNotification = "nots.selrolenotification";
    public static String cFDBSelectTemplateField = "nots.seltemplatefield";
    public static String cFDBInMailSent = "nots.inemailsent";
    public static String cFDBSelEmailByReff = "nots.selemailsentbyref";

    private static List<JMTOEmailTemplater> templateLoaderList;
    private final JSONObject mrequest;

    public abstract JSONObject performHandler(JSONObject pRequestMessage) throws JSONException;

    public static List<JMTOEmailTemplater> getTemplateLoaderList() {
        return null;
    }

    public static void setTemplateLoaderList(List<JMTOEmailTemplater> templateLoaderList) {
        JMTONotifcationHandler.templateLoaderList = templateLoaderList;
    }

    public JMTONotifcationHandler(JSONObject mRequest) {
        this.mrequest = mRequest;
    }

    public void handleMessage() {
        performHandler(mrequest);
    }

    public JMTOEmailTemplater getTemplateWithKey(String pTemplateKey){
        JSONObject tDataField = QueryService.selectTemplateHandler(pTemplateKey);
        String tCode = tDataField.getString("code");
        String tName = tDataField.getString("name");
        String tHtmlTemplate = tDataField.getString("content_html");
        String tCssTemplate = tDataField.getString("content_css");
        String tSubject = tDataField.getString("subject");
        String tAttachment = tDataField.getString("attachment");
        String tFieldData = tDataField.getString("syntax_fields");
        String[] tFieldList = tFieldData.split(";");

        LinkedList<TemplateField> tTemplateFiledLiist = new LinkedList<>();
        for (String fieldtemplate : tFieldList) {
            TemplateField field = new TemplateField(fieldtemplate);
            tTemplateFiledLiist.add(field);
        }
        
        return new JMTOEmailTemplater(new TemplateKey(tTemplateFiledLiist, tCode, tName, tSubject, tHtmlTemplate, tCssTemplate, tAttachment));
    }

}
