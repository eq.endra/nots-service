package id.co.jmto.nots.plugin.akses;

import id.co.jmto.nots.plugin.common.General;
import id.co.jmto.nots.plugin.handler.JMTONotifcationHandler;
import id.co.jmto.nots.plugin.enums.tables.DBEmailSent;
import id.co.jmto.nots.plugin.enums.NotificationMessage;
import id.co.jmto.nots.plugin.enums.tables.DBAppNots;
import id.co.jmto.nots.plugin.enums.tables.NotsLogs;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import verso.loader.AppException;
import verso.message.ResponseCode;

public class QueryService extends General{
    
    public static void logMailMessage(JSONObject pRequestMessage, String pContent, String pSubject, String[] pDestnum) {
        HashMap<String, String> tParam = new HashMap<>();

        tParam.put("USER_ID", pRequestMessage.getString(NotificationMessage.USERID.getFieldName()));
        tParam.put(DBEmailSent.MAIL_TO.getFieldName(), Arrays.toString(pDestnum));
        tParam.put(DBEmailSent.CONTENT_HTML.getFieldName(), pContent.replace("'", "\\\""));
        tParam.put(DBEmailSent.MAIL_SUBJECT.getFieldName(), pSubject);

        JSONObject tReqParam = new JSONObject(tParam);
        Connection.sendAsync(JMTONotifcationHandler.cFDBInMailSent, tReqParam);
    }
    
    public static String getTemplate(String pID) {
        String tReturn = "";
        try {
            HashMap<String, String> tParam = new HashMap<>();
            tParam.put("id", pID);
            JSONObject tReqParam = new JSONObject(tParam);
            JSONObject tRespDB = Connection.getJSONObjectValue(JMTONotifcationHandler.cFDBSelectTemplate, tReqParam);
            if (tRespDB != null) {
                tReturn = tRespDB.getString("TEMPLATE");
            }
        } catch (Exception e) {
            errorLog("failed response in getting template: ", e);
        }

        return tReturn;
    }
    
    public static JSONArray getRoleNotification(String pID){
        HashMap<String, String> tParam = new HashMap<>();
        tParam.put(NotificationMessage.GROUP_NOTIF_ID.getFieldName(), pID);
        JSONObject tReqParam = new JSONObject(tParam);
        JSONArray tRespDB = Connection.getJSONArrayValue(JMTONotifcationHandler.cFDBSelectRoleNotification, tReqParam);
        if (tRespDB != null) {
            if (tRespDB.length() > 0) {
                return tRespDB;
            }
        }
        
        throw new AppException(ResponseCode.ERROR_UNREGISTERED_PRODUCT, "Invalid Code "+pID);
    }
    
    public static JSONArray selectTemplateHandler(){
        JSONArray tRespDB;
        try {
            HashMap<String, String> tParam = new HashMap<>();
            JSONObject tReqParam = new JSONObject(tParam);
            tRespDB = Connection.getJSONArrayValue(JMTONotifcationHandler.cFDBSelectTemplate, tReqParam);
        } catch (AppException ex) {
            return new JSONArray();
        }
        return tRespDB;
    }
    
    public static JSONObject selectTemplateHandler(String pTemplateKey){
        JSONObject tRespDB;
        HashMap<String, String> tParam = new HashMap<>();
        JSONObject tReqParam = new JSONObject(tParam);
        tReqParam.put("KEY", pTemplateKey);
        tRespDB = Connection.getJSONObjectValue(JMTONotifcationHandler.cFDBSelectTemplateByKey, tReqParam);

        if (tRespDB == null) {
            throw new AppException(ResponseCode.ERROR_INVALID_KEY, "Cannot Find Tempalte " + pTemplateKey);
        }
        return tRespDB;
    }

    public static void logJSONMessage(JSONObject pRequestMessage) {
        if (General.getBooleanConfig(cStresTestKey)) {
            return;
        }
        
        try {
            HashMap<String, String> tParam = new HashMap<>();
            tParam.put(NotsLogs.USER_ID.getFieldName(true), pRequestMessage.getString(NotificationMessage.USERID.getFieldName()));
            tParam.put(NotsLogs.MESSAGE_TYPE.getFieldName(true), pRequestMessage.getString(NotificationMessage.MSGTYPE.getFieldName()));
            tParam.put(NotsLogs.MSG_DEST.getFieldName(true), pRequestMessage.has(NotificationMessage.CONTENT_EMAIL.getFieldName()) ?
                    pRequestMessage.getJSONObject(NotificationMessage.CONTENT_EMAIL.getFieldName()).getString(NotificationMessage.DESTNUM.getFieldName()) : "");
            String tLogged = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String tWID = String.valueOf(Thread.currentThread().getId());
            String tRespCode = pRequestMessage.has(NotificationMessage.RC.getFieldName()) ? pRequestMessage.getString(NotificationMessage.RC.getFieldName()) : "-";
            tParam.put(NotsLogs.LOGGED.getFieldName(true), tLogged);
            tParam.put(NotsLogs.WID.getFieldName(true), tWID);
            tParam.put(NotsLogs.RC.getFieldName(true), tRespCode);
            tParam.put(NotsLogs.CONTENT.getFieldName(true), pRequestMessage.toString());
            tParam.put(NotsLogs.ID.getFieldName(true), UUID.randomUUID().toString().replace("-", ""));
            
            JSONObject tReqParam = new JSONObject(tParam);
            Connection.sendAsync(JMTONotifcationHandler.cFDBInsertLog, tReqParam);
        
        } catch (JSONException e) {
            warningLog("Cannot Inset Message Logs. {}", e);
        }
    }

    public static void insertNotificationApps(String pContent, String pRoleID, String pType) {
        HashMap<String, String> tParam = new HashMap<>();
        tParam.put(DBAppNots.TYPE.getFieldName(), pType);
        tParam.put(DBAppNots.ROLE_ID.getFieldName(), pRoleID);
        tParam.put(DBAppNots.CONTENT.getFieldName(), pContent);

        JSONObject tReqParam = new JSONObject(tParam);
        Connection.sendsync(JMTONotifcationHandler.cFDBInsertNotsWebApp, tReqParam);
    }
}