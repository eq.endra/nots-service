/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.handler.app;

import id.co.jmto.nots.plugin.handler.email.*;
import id.co.jmto.nots.plugin.akses.QueryService;
import id.co.jmto.nots.plugin.common.General;
import id.co.jmto.nots.plugin.enums.NotificationMessage;
import id.co.jmto.nots.plugin.handler.JMTONotifcationHandler;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import verso.loader.AppException;
import verso.message.ResponseCode;

/**
 *
 * @author RCS
 */
public class NotifAppslHandler extends JMTONotifcationHandler {

    public NotifAppslHandler(JSONObject pRequest) {
        super(pRequest);
    }

    @Override
    public JSONObject performHandler(JSONObject pRequestMessage) throws JSONException {
        String tContentkey = "";
        List<JMTOEmailTemplater> tSubPluginLoaded = getTemplateLoaderList();
        JSONObject tContent = new JSONObject(pRequestMessage.get(NotificationMessage.CONTENT_WEB_APP.getFieldName()).toString());
        JSONArray tRoleNots = QueryService.getRoleNotification(tContent.get(NotificationMessage.GROUP_NOTIF_ID.getFieldName()).toString());
        if (tSubPluginLoaded != null) {
            boolean isLoaded = false;
            for (JMTOEmailTemplater tPlugin : tSubPluginLoaded) {
                if (tPlugin.canHandleRequest(tContent)) {
                    tContentkey = tPlugin.constructAppNotificationContent(tContent);
                    isLoaded = true;
                    break;
                }
            }
            
            if (!isLoaded) {
                throw new AppException(ResponseCode.ERROR_INVALID_KEY, "Cannot Find Template For Request: "+ tContent.getString("TEMPLATE_KEY"));
            }
        }else{
            JMTOEmailTemplater tPlugin = getTemplateWithKey(tContent.getString("TEMPLATE_KEY"));
            tContentkey = tPlugin.constructAppNotificationContent(tContent);
        }
        
        General.traceLog("RequestMessage: " + pRequestMessage);
        General.traceLog("Content: " + tContentkey);
        for (int i=0; i <  tRoleNots.length(); i++) {
            JSONObject tData = tRoleNots.getJSONObject(i);
            QueryService.insertNotificationApps(tContentkey, tData.get("role_id").toString(), tData.get("jenis_notifikasi_id").toString());
        }
        
        JSONObject tRespMessage = pRequestMessage;

        tRespMessage.put(NotificationMessage.RC.getFieldName(), ResponseCode.SUCCESS.getResponseCodeString());
        tRespMessage.put("RCM", "SUKSES");

        return tRespMessage;
    }

}
