/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.jmto.nots.plugin.common;

/**
 *
 * @author RCS
 */
public class TemplateField {
    private final String field;

    public TemplateField(String field) {
        this.field = field;
    }
    
    public String getField(){
        return field.replace("[", "").replace("]", "");
    }
    
    public String getReplacedField(){
        return "#"+getField()+"#";
    }
}
