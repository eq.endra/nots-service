/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tester;

import id.co.jmto.nots.plugin.common.General;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author RCS
 */
public class Tester {
    private final Connection mConection;

    public Tester() {
        mConection = initConection();
    }
    
    private Connection initConection(){
        try {
            Class.forName(org.postgresql.Driver.class.getName());
            Connection con = DriverManager.getConnection("jdbc:postgresql://10.168.26.10:5432/JMTO", "postgres", "postgres");
            System.out.println("Done Initialize Connection: "+ con.getCatalog());
            return con;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    private void runingTask(List<String[]> tData, int Start) throws SQLException{
        int n = Start;
        for (String[] data : tData) {
            String tSQL = "INSERT INTO \"transaction\".app_template (created_at,created_by,updated_at,updated_by,deleted_at,deleted_by,isactive,code,\"name\",subject,content_html,content_css,syntax_data,syntax_fields,attachment) VALUES " +
"(NOW(),NULL,NULL,NULL,NULL,NULL,false,'JMET"+General.leftPadding(String.valueOf(n), 3, "0")+"','"+data[0]+"','"+data[1]+"','<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" style=\"width:100%;font-family:''Open Sans'', sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0\"> \n" +
" <head> \n" +
"  <meta charset=\"UTF-8\"> \n" +
"  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> \n" +
"  <meta name=\"x-apple-disable-message-reformatting\"> \n" +
"  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> \n" +
"  <meta content=\"telephone=no\" name=\"format-detection\"> \n" +
"  <title>New Template 2</title>\n" +
"  <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\" rel=\"stylesheet\"> \n" +
"  <link href=\"https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i\" rel=\"stylesheet\">\n" +
"  #style#\n" +
" </head> \n" +
" <body style=\"width:100%;font-family:''Open Sans'', sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0\"> \n" +
"  <div class=\"es-wrapper-color\" style=\"background-color:#EFF2F7\">\n" +
"   <table class=\"es-wrapper\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top\"> \n" +
"     <tr style=\"border-collapse:collapse\"> \n" +
"      <td valign=\"top\" style=\"padding:0;Margin:0\"> \n" +
"       <table class=\"es-content\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%\"> \n" +
"         <tr style=\"border-collapse:collapse\"> \n" +
"          <td align=\"center\" style=\"padding:0;Margin:0\"> \n" +
"           <table class=\"es-content-body\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#082758\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#082758;width:600px\"> \n" +
"             <tr style=\"border-collapse:collapse\"> \n" +
"              <td align=\"left\" style=\"Margin:0;padding-left:15px;padding-right:15px;padding-top:20px;padding-bottom:20px\"> \n" +
"               <table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                 <tr style=\"border-collapse:collapse\"> \n" +
"                  <td class=\"es-m-p0r\" valign=\"top\" align=\"center\" style=\"padding:0;Margin:0;width:570px\"> \n" +
"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" role=\"presentation\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" style=\"padding:0;Margin:0;padding-bottom:15px\"><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:''Open Sans'', sans-serif;line-height:21px;color:#8492A6;font-size:14px\">JMTO Payment Gateway</p></td> \n" +
"                     </tr> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;font-size:0px\"><a target=\"_blank\" href=\"\" style=\"-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#0C66FF;font-size:14px\"><img src=\"\" alt style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\" width=\"150\"></a></td> \n" +
"                     </tr> \n" +
"                   </table></td> \n" +
"                 </tr> \n" +
"               </table></td> \n" +
"             </tr> \n" +
"           </table></td> \n" +
"         </tr> \n" +
"       </table> \n" +
"       <table class=\"es-content\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%\"> \n" +
"         <tr style=\"border-collapse:collapse\"> \n" +
"          <td align=\"center\" style=\"padding:0;Margin:0\"> \n" +
"           <table class=\"es-content-body\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#fefefe\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FEFEFE;width:600px\"> \n" +
"             <tr style=\"border-collapse:collapse\"> \n" +
"              <td align=\"left\" style=\"Margin:0;padding-left:15px;padding-right:15px;padding-top:40px;padding-bottom:40px\"> \n" +
"               <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                 <tr style=\"border-collapse:collapse\"> \n" +
"                  <td align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;width:570px\"> \n" +
"                   <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" role=\"presentation\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" style=\"padding:0;Margin:0\">\n" +
"                        <h1 style=\"Margin:0;line-height:31px;mso-line-height-rule:exactly;font-family:roboto, ''helvetica neue'', helvetica, arial, sans-serif;font-size:26px;font-style:normal;font-weight:bold;color:#3C4858\">\n" +
"                          " + data[2] +
"                        </h1>\n" +
"                      </td> \n" +
"                     </tr> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" style=\"padding:0;Margin:0;padding-top:10px;font-size:0px\"><img class=\"adapt-img\" src=\"\" alt style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\" width=\"280\"></td> \n" +
"                     </tr> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" style=\"padding:0;Margin:0;padding-top:10px\">\n" +
"                        <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:''Open Sans'', sans-serif;line-height:21px;color:#8492A6;font-size:14px\">" +
"                          <b>KONTEN EMAIL IS HERE</b><br>" +
"                          #NO_PARAM#\n" +
"                        </p>\n" +
"                      </td> \n" +
"                     </tr> \n" +
"                   </table></td> \n" +
"                 </tr> \n" +
"               </table></td> \n" +
"             </tr> \n" +
"           </table></td> \n" +
"         </tr> \n" +
"       </table> \n" +
"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%\"> \n" +
"         <tr style=\"border-collapse:collapse\"> \n" +
"          <td align=\"center\" style=\"padding:0;Margin:0\"> \n" +
"           <table class=\"es-content-body\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#0a3b6d\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#0a3b6d;width:600px\"> \n" +
"             <tr style=\"border-collapse:collapse\"> \n" +
"              <td align=\"left\" style=\"Margin:0;padding-left:15px;padding-right:15px;padding-top:40px;padding-bottom:40px\"><!--[if mso]><table style=\"width:570px\" cellpadding=\"0\" \n" +
"                        cellspacing=\"0\"><tr><td style=\"width:180px\" valign=\"top\"><![endif]--> \n" +
"               <table class=\"es-left\" cellspacing=\"0\" cellpadding=\"0\" align=\"left\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left\"> \n" +
"                 <tr style=\"border-collapse:collapse\"> \n" +
"                  <td class=\"es-m-p20b\" align=\"left\" style=\"padding:0;Margin:0;width:180px\"> \n" +
"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" role=\"presentation\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;font-size:0px\"><a target=\"_blank\" href=\"\" style=\"-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#0C66FF;font-size:14px\"><img src=\"\" alt style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\" width=\"150\"></a></td> \n" +
"                     </tr> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;padding-top:20px;font-size:0\"> \n" +
"                       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-table-not-adapt es-social\" role=\"presentation\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                         <tr style=\"border-collapse:collapse\"> \n" +
"                          <td align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;padding-right:10px\"><img src=\"https://tsrdpf.stripocdn.email/content/assets/img/social-icons/logo-white/facebook-logo-white.png\" alt=\"Fb\" title=\"Facebook\" width=\"32\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\"></td> \n" +
"                          <td align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;padding-right:10px\"><img src=\"https://tsrdpf.stripocdn.email/content/assets/img/social-icons/logo-white/twitter-logo-white.png\" alt=\"Tw\" title=\"Twitter\" width=\"32\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\"></td> \n" +
"                          <td align=\"center\" valign=\"top\" style=\"padding:0;Margin:0;padding-right:10px\"><img src=\"https://tsrdpf.stripocdn.email/content/assets/img/social-icons/logo-white/youtube-logo-white.png\" alt=\"Yt\" title=\"Youtube\" width=\"32\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\"></td> \n" +
"                          <td align=\"center\" valign=\"top\" style=\"padding:0;Margin:0\"><img src=\"https://tsrdpf.stripocdn.email/content/assets/img/social-icons/logo-white/instagram-logo-white.png\" alt=\"Ig\" title=\"Instagram\" width=\"32\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic\"></td> \n" +
"                         </tr> \n" +
"                       </table></td> \n" +
"                     </tr> \n" +
"                   </table></td> \n" +
"                 </tr> \n" +
"               </table><!--[if mso]></td><td style=\"width:20px\"></td><td style=\"width:370px\" valign=\"top\"><![endif]--> \n" +
"               <table class=\"es-right\" cellspacing=\"0\" cellpadding=\"0\" align=\"right\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right\"> \n" +
"                 <tr style=\"border-collapse:collapse\"> \n" +
"                  <td align=\"left\" style=\"padding:0;Margin:0;width:370px\"> \n" +
"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" role=\"presentation\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"left\" class=\"es-m-txt-c\" style=\"padding:0;Margin:0;padding-top:20px;padding-bottom:20px\">\n" +
"                        <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:''Open Sans'', sans-serif;line-height:21px;color:#8492A6;font-size:12px\">\n" +
"                        <b>Kebijakan Privasi dan Keamanan</b><br><br>\n" +
"JMTO tidak akan pernah meminta anda untuk memberi tahu kata sandi atau informasi akun pribadi anda kepada kami melalui email. Anda hanya akan diminta untuk memasukkan password anda ketika anda masuk ke website kami. Kami menggunakan langkah-langkah untuk memastikan keamanan anda menggunakan layanan PG kami dan melindungi kerahasiaan informasi pribadi yang anda berikan kepada kami. Kami juga melakukan segala upaya untuk memastikan email-email yang kami kirimkan telah melalui proses pengecekan virus sebelum dikirimkan. Jika anda menerima email yang mencurigakan atau terjadi kesalahan tujuan pengiriman, mohon laporkan hal tersebut kepada Tim Layanan Pelanggan kami di contact (021) 22984722 untuk penyelidikan lebih lanjut .\n" +
"                        </p></td> \n" +
"                     </tr> \n" +
"                   </table></td> \n" +
"                 </tr> \n" +
"               </table><!--[if mso]></td></tr></table><![endif]--></td> \n" +
"             </tr> \n" +
"           </table></td> \n" +
"         </tr> \n" +
"       </table> \n" +
"       <table cellpadding=\"0\" cellspacing=\"0\" class=\"es-content\" align=\"center\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%\"> \n" +
"         <tr style=\"border-collapse:collapse\"> \n" +
"          <td align=\"center\" style=\"padding:0;Margin:0\"> \n" +
"           <table bgcolor=\"transparent\" class=\"es-content-body\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px\"> \n" +
"             <tr style=\"border-collapse:collapse\"> \n" +
"              <td align=\"left\" style=\"Margin:0;padding-left:20px;padding-right:20px;padding-top:30px;padding-bottom:30px\"> \n" +
"               <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                 <tr style=\"border-collapse:collapse\"> \n" +
"                  <td valign=\"top\" align=\"center\" style=\"padding:0;Margin:0;width:560px\"> \n" +
"                   <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px\"> \n" +
"                     <tr style=\"border-collapse:collapse\"> \n" +
"                      <td align=\"center\" style=\"padding:0;Margin:0;display:none\"></td> \n" +
"                     </tr> \n" +
"                   </table></td> \n" +
"                 </tr> \n" +
"               </table></td> \n" +
"             </tr> \n" +
"           </table></td> \n" +
"         </tr> \n" +
"       </table></td> \n" +
"     </tr> \n" +
"   </table> \n" +
"  </div>  \n" +
" </body>\n" +
"</html>','#outlook a {\n" +
"	padding:0;\n" +
"}\n" +
".ExternalClass {\n" +
"	width:100%;\n" +
"}\n" +
".ExternalClass,\n" +
".ExternalClass p,\n" +
".ExternalClass span,\n" +
".ExternalClass font,\n" +
".ExternalClass td,\n" +
".ExternalClass div {\n" +
"	line-height:100%;\n" +
"}\n" +
".es-button {\n" +
"	mso-style-priority:100!important;\n" +
"	text-decoration:none!important;\n" +
"}\n" +
"\n" +
".a {\n" +
"  text-decoration: none;\n" +
"}\n" +
"\n" +
"a[x-apple-data-detectors] {\n" +
"	color:inherit!important;\n" +
"	text-decoration:none!important;\n" +
"	font-size:inherit!important;\n" +
"	font-family:inherit!important;\n" +
"	font-weight:inherit!important;\n" +
"	line-height:inherit!important;\n" +
"}\n" +
".es-desk-hidden {\n" +
"	display:none;\n" +
"	float:left;\n" +
"	overflow:hidden;\n" +
"	width:0;\n" +
"	max-height:0;\n" +
"	line-height:0;\n" +
"	mso-hide:all;\n" +
"}\n" +
"[data-ogsb] .es-button {\n" +
"	border-width:0!important;\n" +
"	padding:15px 30px 15px 30px!important;\n" +
"}\n" +
"@media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1, h2, h3, h1 a, h2 a, h3 a { line-height:120% } h1 { font-size:28px!important; text-align:left } h2 { font-size:20px!important; text-align:left } h3 { font-size:14px!important; text-align:left } h1 a { text-align:left } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:28px!important } h2 a { text-align:left } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:20px!important } h3 a { text-align:left } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:14px!important } .es-menu td a { font-size:14px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:14px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:14px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:14px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:14px!important } *[class=\"gmail-fix\"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button, button.es-button { font-size:14px!important; display:block!important; border-bottom-width:20px!important; border-right-width:0px!important; border-left-width:0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }','[NO_PARAM]','[NO_PARAM]','')";
        
            Statement stat = mConection.createStatement();
            stat.execute(tSQL);
            n++;
        }
        
    }
    
    public static void main(String[] args) {
        try {
            Tester tester = new Tester();
            List<String[]> tList = new ArrayList<>();
            tList.add(new String[]{"Request Manual Pencairan Dana", "[Request] Manual Pencairan Dana", "Pencairan Dana - Manual"});
            tList.add(new String[]{"Request Auto Pencairan Dana", "[Request] Auot Pencairan Dana", "Pencairan Dana - Auto"});
            tList.add(new String[]{"Status Request Manual Pencairan Dana", "[Status] Request Manual Pencairan Dana", "Status Pencairan Dana - Manual"});
            tList.add(new String[]{"Status Request Auto Pencairan Dana", "[Status] Request Auto Pencairan Dana", "Status Pencairan Dana - Auto"});
            tList.add(new String[]{"Status Refund Merchant", "[Status] Refund Merchant", "Status Refund - Merchant"});
            tList.add(new String[]{"Format Dan Mandatory Field", "Format dan Mandatory Field", "Format dan Mandatory Field"});
            tList.add(new String[]{"Perubahan Tarif", "Perubahan Tarif Efektif", "Perubahan Tarif Efektif"});
            tList.add(new String[]{"Role FDS", "[Alert] Role FDS", "Role FDS"});
            tList.add(new String[]{"Request Pencairan Dana Merchant dan Submerchant", "Request Manual Pencairan Dana", "Pencairan Dana Merchant/Sub Merchant"});
            tList.add(new String[]{"Pendaftaran Submerchant", "Pendaftaran Sub Merchant", "Pendaftaran Sub Merchant"});
            tList.add(new String[]{"Registrasi Kelengkapan Data Merchant", "[Request] Registrasi Kelengkapan Data Merchant", "Registrasi Kelengkapan Data"});
            tList.add(new String[]{"Request Perubahan Data Data Merchant", "[Request] Perubahan Data Merchant", "Perubahan Data"});
            tList.add(new String[]{"Request Aktivasi Payment Merchant dan SOF", "[Request] Aktivasi Payment Chanel/SOF", "Aktivasi Payment Chanel/SOF"});
            tList.add(new String[]{"Status Aktivasi Payment Merchant dan SOF", "[Status] Aktivasi Payment Chanel/SOF", "Status Aktivasi Payment Chanel/SOF"});
            tList.add(new String[]{"Status Pencairan Dana Dari SOF", "Pencairan Dana", "Status Pencairan Dana"});
            tList.add(new String[]{"Status refund ke arah SOF yang diajukan role settlement", "[Status] Refund From SOF", "Status Refund"});
            tList.add(new String[]{"Selisih Hasil Rekonsiliasi", "Selisih Hasil Rekon", "Selisih Hasil Rekonsiliasi"});
            tList.add(new String[]{"FDS Alert", "FDS ALert", "Peringatan"});
            tList.add(new String[]{"Registrasi User Login SOF", "Registrasi User Login - SOF", "Registrasi User Login"});
            tList.add(new String[]{"Blokir Akses Logib sub SOF", "Blokir Akses Login - SOF", "Pemblokiran Akses Login - SOF"});
            tList.add(new String[]{"Reset Password SOF", "Reset Password - SOF", "Reset Password - SOF"});
            tList.add(new String[]{"Ganti Password SOF", "Ganti Password - SOF", "Ganti Password - SOF"});
            tList.add(new String[]{"Registrasi User Login SOF", "Registrasi User Login - SOF", "User Login - SOF"});
            tList.add(new String[]{"Perubahan tarif SOF", "Perubahan Tarif - SOF", "Perubahan Tarif - SOF"});
            tList.add(new String[]{"Selisih Hasil Rekonsiliasi SOF", "Selisih Hasil Rekonsiliasi - SOF", "Selisih Hasil Rekonsiliasi - SOF"});
            tList.add(new String[]{"Registrasi User Login Sub Merchant", "Registrasi User Login - Sub Merchant", "Registrasi User Login"});
            tList.add(new String[]{"Registrasi Kelengkapan Data Sub Merchant", "[Request] Registrasi Kelengkapan Data Sub Merchant", "Registrasi Kelengkapan Data"});
            tList.add(new String[]{"Reminder Registrasi Kelengkapan Data Sub Merchant", "[Reminder] Registrasi Kelengkapan Data Sub Merchant", "Registrasi Kelengkapan Data"});
            tList.add(new String[]{"Status Registrasi Kelengkapan Data Sub Merchant", "[Status] Registrasi Kelengkapan Data Sub Merchant", "Registrasi Kelengkapan Data"});
            tList.add(new String[]{"Blokir Akses Login Sub Merchant", "Blokir Akses Sub Merchant", "Pemblokiran Akses Login"});
            tList.add(new String[]{"Reset Password Sub Merchant", "Reset Password", "Reset Password"});
            tList.add(new String[]{"Ganti Password Sub Merchant", "Ganti Password", "Ganti Password"});
            tList.add(new String[]{"Pembuatan User Login Sub Merchant", "Pembuatan User Login - Sub Merchant", "User Login - Sub Merchant"});
            tList.add(new String[]{"Request Manual Pencairan Dana Sub Merchant", "[Request] manual Pencairan Dana - Sub Merchant", "Manual Pencairan Dana"});
            tList.add(new String[]{"Status Manual Pencairan Dana Sub Merchant", "[Status] manual Pencairan Dana - Sub Merchant", "Manual Pencairan Dana"});
            tList.add(new String[]{"Status Auto Pencairan Dana Sub Merchant", "[Status] Auto Pencairan Dana - Sub Merchant", "Auto Pencairan Dana"});
            tList.add(new String[]{"Refund Dana Sub Merchant ke user", "Pengembalian Dana User", "Pengembalian Dana"});
            tester.runingTask(tList, 10);
        } catch (SQLException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
